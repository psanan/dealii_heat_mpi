// PDS: Modified by Patrick Sanan, May 2015
// PDS: Modifications should be commented like this
// PDS: Original Copyright notice:
/* ---------------------------------------------------------------------
 *
 * Copyright (C) 2013 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------

 *
 * Author: Wolfgang Bangerth, Texas A&M University, 2013
 *
 */


// The program starts with the usual include files, all of which you should
// have seen before by now:
#include <deal.II/base/utilities.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/solution_transfer.h>
#include <deal.II/numerics/matrix_tools.h>

#include <fstream>
#include <iostream>

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/utilities.h>
#include <deal.II/lac/petsc_vector.h>
#include <deal.II/lac/petsc_parallel_vector.h>
#include <deal.II/lac/petsc_parallel_sparse_matrix.h>
#include <deal.II/lac/petsc_solver.h>
#include <deal.II/lac/petsc_precondition.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <sstream>

namespace Step26
{
  using namespace dealii;
  
template<int dim>
  class RightHandSide : public Function<dim>
  {
  public:
    RightHandSide ()
      :
      Function<dim>(),
      period (0.2)
    {}

    virtual double value (const Point<dim> &p,
                          const unsigned int component = 0) const;

  private:
    const double period;
  };
  
  template<int dim>
  class HeatEquation
  {
  public:
    HeatEquation();
    ~HeatEquation();
    void run();

  private:
    void setup_system();
    void assemble_system();
    void assemble_F(const RightHandSide<dim>&, PETScWrappers::MPI::Vector&);
    int  solve_time_step();
    void output_results() const;
    //PDS: ignore refinement
    /*
    void refine_mesh (const unsigned int min_grid_level,
                      const unsigned int max_grid_level);
     */

    Triangulation<dim>   triangulation;
    FE_Q<dim>            fe;
    DoFHandler<dim>      dof_handler;

    //PDS: We change the name of this member, as in the example
    //PDS:  there were no other explicit constraints
    ConstraintMatrix     hanging_node_constraints;

    double               time;
    const double         time_step; // PDS : we make this constant here
    unsigned int         timestep_number;

    const double         theta;

    // PDS: replace these data structures with PETSc ones
    PETScWrappers::MPI::SparseMatrix mass_matrix;
    PETScWrappers::MPI::SparseMatrix laplace_matrix;
    PETScWrappers::MPI::SparseMatrix system_matrix;
    PETScWrappers::MPI::Vector solution;
    PETScWrappers::MPI::Vector old_solution;
    PETScWrappers::MPI::Vector system_rhs;

    //PDS: data concerning an mpi communicator
    MPI_Comm mpi_communicator;
    const unsigned int n_mpi_processes;
    const unsigned int this_mpi_process;

    //PDS: a rank-0 output stream
    ConditionalOStream pcout;
  };

  template<int dim>
  double RightHandSide<dim>::value (const Point<dim> &p,
                                    const unsigned int component) const
  {
    Assert (component == 0, ExcInternalError());
    Assert (dim == 2, ExcNotImplemented());

    const double time = this->get_time();
    const double point_within_period = (time/period - std::floor(time/period));

    if ((point_within_period >= 0.0) && (point_within_period <= 0.2))
      {
        if ((p[0] > 0.5) && (p[1] > -0.5))
          return 1;
        else
          return 0;
      }
    else if ((point_within_period >= 0.5) && (point_within_period <= 0.7))
      {
        if ((p[0] > -0.5) && (p[1] > 0.5))
          return 1;
        else
          return 0;
      }
    else
      return 0;
  }

  template<int dim>
  class BoundaryValues : public Function<dim>
  {
  public:
    virtual double value (const Point<dim>  &p,
                          const unsigned int component = 0) const;
  };

  template<int dim>
  double BoundaryValues<dim>::value (const Point<dim> &/*p*/,
                                     const unsigned int component) const
  {
    Assert(component == 0, ExcInternalError());
    return 0;
  }

  template<int dim>
  HeatEquation<dim>::HeatEquation ()
    :
    fe(1),
    dof_handler(triangulation),
    time_step(1. / 500),
    theta(0.5),

    // PDS: Initialize additional data members related to MPI
    mpi_communicator (MPI_COMM_WORLD),
    n_mpi_processes (Utilities::MPI::n_mpi_processes(mpi_communicator)),
    this_mpi_process (Utilities::MPI::this_mpi_process(mpi_communicator)),
    pcout (std::cout)
  {
    pcout.set_condition(this_mpi_process == 0);
  }

  // PDS: Add destructor
  template <int dim>
  HeatEquation<dim>::~HeatEquation ()
  {
    dof_handler.clear ();
  }

  template<int dim>
  void HeatEquation<dim>::setup_system()
  {

    //PDS:
    GridTools::partition_triangulation (n_mpi_processes, triangulation);
    dof_handler.distribute_dofs (fe);

    //PDS:
    DoFRenumbering::subdomain_wise (dof_handler);
    const types::global_dof_index n_local_dofs
      = DoFTools::count_dofs_with_subdomain_association (dof_handler,
                                                     this_mpi_process);

    //PDS: cout -- > pcout
    pcout << std::endl
              << "==========================================="
              << std::endl
              << "Number of active cells: " << triangulation.n_active_cells()
              << std::endl
              << "Number of degrees of freedom: " << dof_handler.n_dofs()
              << std::endl
              << std::endl;
    
    //PDS: much changes below
    system_matrix.reinit (mpi_communicator,
                          dof_handler.n_dofs(),
                          dof_handler.n_dofs(),
                          n_local_dofs,
                          n_local_dofs,
                          dof_handler.max_couplings_between_dofs());
    laplace_matrix.reinit (mpi_communicator,
                          dof_handler.n_dofs(),
                          dof_handler.n_dofs(),
                          n_local_dofs,
                          n_local_dofs,
                          dof_handler.max_couplings_between_dofs());
    mass_matrix.reinit (mpi_communicator,
                          dof_handler.n_dofs(),
                          dof_handler.n_dofs(),
                          n_local_dofs,
                          n_local_dofs,
                          dof_handler.max_couplings_between_dofs());
    solution.reinit     (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);
    old_solution.reinit (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);
    system_rhs.reinit   (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);

    //PDS: Note name change
    hanging_node_constraints.clear ();
    DoFTools::make_hanging_node_constraints (dof_handler,
                                             hanging_node_constraints);
    hanging_node_constraints.close();

    // PDS: Note that it remains to assemble the system
  }

  //PDS: assemble_system assembles three matrices, assuming a *constant time step and theta*
  //PDS:  (this constant nature is explicitly enforced in the class definition)
  template <int dim>
  void HeatEquation<dim>::assemble_system ()
  {
    QGauss<dim>  quadrature_formula(2);
    FEValues<dim> fe_values (fe, quadrature_formula,
                             update_values   | update_gradients |
                             update_quadrature_points | update_JxW_values);
    const unsigned int   dofs_per_cell = fe.dofs_per_cell;
    const unsigned int   n_q_points    = quadrature_formula.size();
    FullMatrix<double>   cell_matrix_laplace (dofs_per_cell, dofs_per_cell);
    FullMatrix<double>   cell_matrix_mass    (dofs_per_cell, dofs_per_cell);
    FullMatrix<double>   cell_matrix_system  (dofs_per_cell, dofs_per_cell);
    Vector<double>       cell_rhs            (dofs_per_cell); // PDS: this is just a placeholder
    std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);


    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();
    for (; cell!=endc; ++cell){
      cell_matrix_laplace = 0;
      cell_matrix_mass    = 0;
      cell_rhs = 0; // PDS : dummy
      fe_values.reinit (cell);
      if (cell->subdomain_id() == this_mpi_process)
      {
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        {
          for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
            {
              cell_matrix_laplace(i,j) += (fe_values.shape_grad (i, q_point) *
                  fe_values.shape_grad (j, q_point) *
                  fe_values.JxW (q_point));
              cell_matrix_mass(i,j) += (fe_values.shape_value(i, q_point) *
                  fe_values.shape_value(j, q_point) *
                  fe_values.JxW (q_point));
            }
          }
        }
        //PDS: the system matrix is built from the mass and laplace matrices,
        //PDS   assuming a constant time step and theta
        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
          for (unsigned int j=0; j<dofs_per_cell; ++j)
          {
            cell_matrix_system(i,j) = 
              cell_matrix_mass(i,j) + (time_step * theta * cell_matrix_laplace(i,j));
          }
        }
        cell->get_dof_indices (local_dof_indices);
        hanging_node_constraints.distribute_local_to_global(
            cell_matrix_system, cell_rhs,
            local_dof_indices,
            system_matrix, system_rhs); //PDS : system_rhs is garbage
        hanging_node_constraints.distribute_local_to_global(
            cell_matrix_mass, cell_rhs,
            local_dof_indices,
            mass_matrix, system_rhs); //PDS : system_rhs *garbage*
        hanging_node_constraints.distribute_local_to_global(
            cell_matrix_laplace, cell_rhs,
            local_dof_indices,
            laplace_matrix, system_rhs); //PDS : system_rhs *garbage*
      }
    }
    laplace_matrix.compress(VectorOperation::add);
    mass_matrix.compress(VectorOperation::add);
    system_matrix.compress(VectorOperation::add);

    //PDS: Note that we do not enforce the boundary values here (but do so later on once
    //PDS   we have the rhs for the system, which varies with the time step)
  }

template<int dim>
  void HeatEquation<dim>::assemble_F (const RightHandSide<dim> &rhs_function,PETScWrappers::MPI::Vector &f)
  {
    QGauss<dim>  quadrature_formula(2);
    FEValues<dim> fe_values (fe, quadrature_formula,
        update_values   | update_gradients |
        update_quadrature_points | update_JxW_values);
    const unsigned int   dofs_per_cell = fe.dofs_per_cell;
    const unsigned int   n_q_points    = quadrature_formula.size();
    std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);
    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();

    {
      const types::global_dof_index n_local_dofs
        = DoFTools::count_dofs_with_subdomain_association (dof_handler,
            this_mpi_process);
      f.reinit (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);
    }

    for (; cell!=endc; ++cell){
      if (cell->subdomain_id() == this_mpi_process)
      {
        cell->get_dof_indices (local_dof_indices);
        fe_values.reinit (cell);
        for (unsigned int q_point=0; q_point<n_q_points; ++q_point)
        {
          for (unsigned int i=0; i<dofs_per_cell; ++i)
          {
              f(local_dof_indices[i]) += 
                fe_values.shape_value (i, q_point) *
                rhs_function.value(fe_values.quadrature_point(q_point)) *
                fe_values.JxW (q_point); 
          }
        }
      }
    }
    f.compress(VectorOperation::add);
  }

  template<int dim>
  //PDS: Note changed return type
  int HeatEquation<dim>::solve_time_step()
  {

    SolverControl           solver_control (solution.size(),
                                            1e-8*system_rhs.l2_norm());
    PETScWrappers::SolverCG cg (solver_control,
                                mpi_communicator);
    // PDS: We use a very simple parallel preconditioner, block jacobi, which
    // PDS:  uses an ILU(0) preconditioner applied to each local block.
    // PDS: The original example used SOR, which is not trivial to parallelize.
    // PDS:Note that this is means the preconditioner *weakens* with core count
    // PDS: (going from 1 to 2 mpi processes will mean the iteration count will change from 4/5 to 8/9)
    PETScWrappers::PreconditionBlockJacobi preconditioner(system_matrix);

    //PDS : you could use a fancier preconditioner (provided there is a wrapper for it)
    //PETScWrappers::PreconditionBoomerAMG preconditioner(system_matrix);
    
    cg.solve (system_matrix, solution, system_rhs,
              preconditioner);

    return solver_control.last_step();
  }

  template<int dim>
  void HeatEquation<dim>::output_results() const
  {

    const PETScWrappers::Vector localized_solution (solution);
    //PDS:  on rank 0 only
    if(!this_mpi_process){
      DataOut<dim> data_out;

      data_out.attach_dof_handler(dof_handler);
      data_out.add_data_vector(localized_solution, "U"); 

      data_out.build_patches();

      const std::string filename = "solution-"
                                   + Utilities::int_to_string(timestep_number, 3) +
                                   ".vtk";
      std::ofstream output(filename.c_str());
      data_out.write_vtk(output);
    }
  }


//PDS: We do no adaptive refinement!
/*
  template <int dim>
  void HeatEquation<dim>::refine_mesh (const unsigned int min_grid_level,
                                       const unsigned int max_grid_level)
  {
    Vector<float> estimated_error_per_cell (triangulation.n_active_cells());

    KellyErrorEstimator<dim>::estimate (dof_handler,
                                        QGauss<dim-1>(fe.degree+1),
                                        typename FunctionMap<dim>::type(),
                                        solution,
                                        estimated_error_per_cell);

    GridRefinement::refine_and_coarsen_fixed_fraction (triangulation,
                                                       estimated_error_per_cell,
                                                       0.6, 0.4);

    if (triangulation.n_levels() > max_grid_level)
      for (typename Triangulation<dim>::active_cell_iterator
           cell = triangulation.begin_active(max_grid_level);
           cell != triangulation.end(); ++cell)
        cell->clear_refine_flag ();
    for (typename Triangulation<dim>::active_cell_iterator
         cell = triangulation.begin_active(min_grid_level);
         cell != triangulation.end_active(min_grid_level); ++cell)
      cell->clear_coarsen_flag ();


    SolutionTransfer<dim> solution_trans(dof_handler);

    Vector<double> previous_solution;
    previous_solution = solution;
    triangulation.prepare_coarsening_and_refinement();
    solution_trans.prepare_for_coarsening_and_refinement(previous_solution);

    triangulation.execute_coarsening_and_refinement ();
    setup_system ();

    solution_trans.interpolate(previous_solution, solution);
  }
*/

  template<int dim>
  void HeatEquation<dim>::run()
  {
    //PDS : choose a set global refinement here
    const unsigned int initial_global_refinement = 3;
    //const unsigned int n_adaptive_pre_refinement_steps = 4;

    GridGenerator::hyper_L (triangulation);
    triangulation.refine_global (initial_global_refinement);

    setup_system();

    // PDS: new assemble_system routine assembles the full operator to be inverted
    // PDS: this implies that we cannot change the timestep or theta
    // PDS: without calling this routine again
    assemble_system();

    unsigned int pre_refinement_step = 0;

    PETScWrappers::MPI::Vector tmp;
    PETScWrappers::MPI::Vector forcing_terms;

start_time_iteration:

    //PDS: reinit with the extra local/global info required
    const types::global_dof_index n_local_dofs
      = DoFTools::count_dofs_with_subdomain_association (dof_handler,
                                                     this_mpi_process);
    tmp.reinit (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);
    forcing_terms.reinit (mpi_communicator, dof_handler.n_dofs(), n_local_dofs);

    //PDS: For now, we ignore adaptive mesh refinement and all the associated complication

    VectorTools::interpolate(dof_handler,
                             ZeroFunction<dim>(),
                             old_solution);

    solution = old_solution;

    timestep_number = 0;
    time            = 0;

    output_results();

    // Then we start the main loop until the computed time exceeds our
    // end time of 0.5. The first task is to build the right hand
    // side of the linear system we need to solve in each time step.
    // Recall that it contains the term $MU^{n-1}-(1-\theta)k_n AU^{n-1}$.
    // We put these terms into the variable system_rhs, with the
    // help of a temporary vector:
    while (time <= 0.5)
      {
        time += time_step;
        ++timestep_number;

        pcout << "Time step " << timestep_number << " at t=" << time
                  << std::endl;

        mass_matrix.vmult(system_rhs, old_solution);

        laplace_matrix.vmult(tmp, old_solution);
        system_rhs.add(-(1 - theta) * time_step, tmp);

        // The second piece is to compute the contributions of the source
        // terms. This corresponds to the term $k_n
        // \left[ (1-\theta)F^{n-1} + \theta F^n \right]$. The following
        // code calls VectorTools::create_right_hand_side to compute the
        // vectors $F$, where we set the time of the right hand side
        // (source) function before we evaluate it. The result of this
        // all ends up in the forcing_terms variable:
        RightHandSide<dim> rhs_function;
        rhs_function.set_time(time);

        //PDS: TVectorTools::create_right_hand_side is not available, so we must define our
        //PDS   own function
        assemble_F(rhs_function,tmp);
        
        forcing_terms = tmp;
        forcing_terms *= time_step * theta;

        rhs_function.set_time(time - time_step);
        assemble_F(rhs_function,tmp);

        forcing_terms.add(time_step * (1 - theta), tmp);
        
        // Next, we add the forcing terms to the ones that
        // come from the time stepping, and also build the matrix
        // $M+k_n\theta A$ that we have to invert in each time step.
        // The final piece of these operations is to eliminate
        // hanging node constrained degrees of freedom from the
        // linear system:
        system_rhs += forcing_terms;


        // PDS system_matrix should have already been assembled (assuming a constant
        // PDS   time step and theta

        // There is one more operation we need to do before we
        // can solve it: boundary values. To this end, we create
        // a boundary value object, set the proper time to the one
        // of the current time step, and evaluate it as we have
        // done many times before. The result is used to also
        // set the correct boundary values in the linear system:

        {
          BoundaryValues<dim> boundary_values_function;
          boundary_values_function.set_time(time);

          std::map<types::global_dof_index, double> boundary_values;
          VectorTools::interpolate_boundary_values(dof_handler,
                                                   0,
                                                   boundary_values_function,
                                                   boundary_values);
          MatrixTools::apply_boundary_values(boundary_values,
                                             system_matrix,
                                             solution,
                                             system_rhs,
                                             false); // PDS : note last argument (see step-17 docs)
        }

        // With this out of the way, all we have to do is solve the
        // system, generate graphical data, and...
        //PDS This returns output now, so output number of iterations
        int niter = solve_time_step();
        
        pcout << "Solve_time_step used " << niter << " iterations" << std::endl;

        output_results();

        // ...take care of mesh refinement. Here, what we want to do is
        // (i) refine the requested number of times at the very beginning
        // of the solution procedure, after which we jump to the top to
        // restart the time iteration, (ii) refine every fifth time
        // step after that.
        //
        // The time loop and, indeed, the main part of the program ends
        // with starting into the next time step by setting old_solution
        // to the solution we have just computed.

        //PDS : We ignore the refinement
        /*
        if ((timestep_number == 1) &&
            (pre_refinement_step < n_adaptive_pre_refinement_steps))
          {
            refine_mesh (initial_global_refinement,
                         initial_global_refinement + n_adaptive_pre_refinement_steps);
            ++pre_refinement_step;

            tmp.reinit (solution.size());
            forcing_terms.reinit (solution.size());

            std::cout << std::endl;

            goto start_time_iteration;
          }
        else if ((timestep_number > 0) && (timestep_number % 5 == 0))
          {
            refine_mesh (initial_global_refinement,
                         initial_global_refinement + n_adaptive_pre_refinement_steps);
            tmp.reinit (solution.size());
            forcing_terms.reinit (solution.size());
          }
*/
        old_solution = solution;
      }
  }
}

int main(int argc, char ** argv)
{
  try
    {
      using namespace dealii;
      using namespace Step26;

      Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);

      deallog.depth_console(0);

      HeatEquation<2> heat_equation_solver;
      heat_equation_solver.run();

    }
  catch (std::exception &exc)
    {
      //PDS: Note that we do nothing to gracefully output the above (no perr), 
      //PDS:  so these messages will likely be garbled if they are encountered on 
      //PDS:  multiple MPI ranks
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl << exc.what()
                << std::endl << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;

      return 1;
    }
  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl << "Aborting!"
                << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}
