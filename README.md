This is an example of solving the heat equation using MPI (with PETSc) in deal.ii

This assumes that you have set up the environment to use deal.ii, for example
with the commands provided in the shell wrapper that comes with the OS X 
dealii.app
